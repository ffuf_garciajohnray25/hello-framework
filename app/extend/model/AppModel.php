<?php

namespace app\extend\model;

/**
 * Description of AppModel
 *
 * @author John Ray Garcia <garcia.johnray25@gmail.com>
 */
class AppModel extends \rueckgrat\mvc\DefaultDBModel {
    
    public function __construct($masterTableName) {
        parent::__construct($masterTableName);
    }

   public function getById($id) {
       if(empty($id)) return;
        $query = "select * from " . $this->masterTableName . " where id=:id";
        $stmnt = $this->db->prepare($query);      
        $stmnt->execute(array(':id' => $id));
        $arr_res = $stmnt->fetch();
        return $arr_res;
    }
    
    public function find( $conditions ) {
        $whereStatement = "";
        $c = $conditions;
        $values = array();
        $last = array_pop($c);
        if(count($conditions)) {
            foreach( $conditions as $key => $condition ) {
                $values[] = $condition;
                if(strtolower($key) === "password") {
                    $whereStatement = $whereStatement . " $key=password(?)";
                } else {
                    $whereStatement = $whereStatement . " $key=?";
                }
                
                if($last != $condition) $whereStatement = $whereStatement . " AND ";
            }
        }
        
        $stmnt = $this->db->prepare( "select * from " . $this->masterTableName . " where " . $whereStatement );
        $stmnt->execute($values);        
        $userData = $stmnt->fetch();
        if($userData) {                         
            return $userData; // Return true to initialize Session.
        }
        return false; // Return false for login failure.
    }
}
