<?php

namespace app\extend\controller;

use rueckgrat\system\Sess as Sess;

/**
 * Description of AppController
 *
 * @author johnrayg
 */
abstract class AppController extends \rueckgrat\mvc\DefaultController {    
    
    public function __construct() {
        parent::__construct();
    }
    
    protected function redirect($controller, $method, $args = null) {
        $arguments = "";
        if($args) {
            foreach ($args as $key=>$arg) {
                $arguments = "&$key=".$arg;
            }
        }
        header("location:". \app\helper\GlobalHelper::base_url() . "?c=". $controller . "&m=".$method . $arguments );
    }
    
    protected function isLoggedIn() {
        if(Sess::exists("currentUserData")) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getCurrentUser() {
        return Sess::get("currentUserData");
    }
    
    public function logOut() {        
        Sess::remove("currentUserData");
        Sess::destroy();        
        $this->redirect("main", "index"); // Redirect to homepage                
    }
}
