<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" type="text/css" href="public/css/style.css" />
        <link rel="stylesheet" type="text/css" href="public/css/bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        [<?php
        foreach ($this->cssFiles AS $file) {
            echo '<link rel="stylesheet" type="text/css" href="' . $file . '" />' . "\n";
        }
        ?>]
    </head>
    <body>
        <div id="wrapper">
            [<?php echo $this->pageContent; ?>]
        </div>
        <?php
        $cb = \rueckgrat\xhr\CallbackManager::getCallbacks();
        if (count($cb) > 0) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . json_encode(array('callbacks' => $cb)) . ');});</script>' . "\n";
        }
        ?>
        [<?php
        foreach ($this->jsFiles AS $file) {
            echo '<script type="text/javascript" src="' . $file . '"></script>' . "\n";
        }
        ?>]
    </body>
</html>