<div id="app_container">
    <header id="appHeader">
        <div class="logo"><span>T-Systems</span></div>
    </header>
    
    <div id="leftContainer">                
        <div class="leftContent">  
            <div class="leftMenuItem">
                <select name="branch">
                    <option value="">Branch</option>
                </select>
            </div>
            <div class="leftMenuItem">
                <select name="department">
                    <option value="">IT</option>
                </select>
            </div>
        </div>
    </div>
    
    <div id="rightContainer">        
        <?php echo $this->menu->render() ?>        
        <p>Hello Administrator!</p>
    </div>
</div>