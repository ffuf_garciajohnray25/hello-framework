<div id="app_container">
    <header id="appHeader">
        <div class="logo"><span>T-Systems</span></div>
    </header>
    
    <div id="leftContainer">                
        <div class="leftContent">  
            <div class="leftMenuItem">
                <select name="branch">
                    <option value="">Branch</option>
                </select>
            </div>
            <div class="leftMenuItem">
                <select name="department">
                    <option value="">IT</option>
                </select>
            </div>
        </div>
    </div>
    
    <div id="rightContainer">        
        <?php echo $this->menu->render() ?>
        <h3>User Management | Create User</h3>
        <form method="post">
        <input type="hidden" name="c" value="AdminPageController">
        <input type="hidden" name="m" value="processAddUser">
        <table width="800px">
            <tr>
                <td>Fore Name</td><td><input type="text" name="Forename" /></td>
            </tr>
            <tr>
                <td>Last Name</td><td><input type="text" name="Lastname" /></td>
            </tr>
            <tr>
                <td>Email</td><td><input type="text" name="Email" /></td>
            </tr>
            </tr>
                <td>Gender</td><td>Male <input type="radio" name="Gender" value="m" /> Female <input type="radio" name="Gender" value="f" /></td>                
            </tr>
            </tr>
                <td>Password</td><td><input type="password" name="password" /></td>                
            </tr>
            </tr>
                <td>Re-type Password</td><td><input type="password" name="password" /></td>                
            </tr>
        </table>
        <input type="submit" value="post">
        </form>
    </div>
</div>
