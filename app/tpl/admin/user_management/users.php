<div id="app_container">
    <header id="appHeader">
        <div class="logo"><span>T-Systems</span></div>
    </header>
    
    <div id="leftContainer">                
        <div class="leftContent">  
            <div class="leftMenuItem">
                <select name="branch">
                    <option value="">Branch</option>
                </select>
            </div>
            <div class="leftMenuItem">
                <select name="department">
                    <option value="">IT</option>
                </select>
            </div>
        </div>
    </div>
    
    <div id="rightContainer">        
        <?php echo $this->menu->render() ?>
        <h3>User Management</h3>
        <table width="800px">
            <tr>
                <td>Fore Name</td>
                <td>Last Name</td>
                <td>Email</td>
                <td>Gender</td>
                <td>Actions</td>
            </tr>
            <?php
                $accounts = $this->accounts;
                
                foreach ($accounts as $account) {
                    echo "<tr>";
                    echo "  <td>".$account->getForename()."</td>";
                    echo "  <td>".$account->getLastname()."</td>";
                    echo "  <td>".$account->getEmail()."</td>";
                    echo "  <td>".$account->getGender()."</td>";
                    echo "  <td><a href='#'>Edit</a> <a href='#'>Delete</a></td>";
                    echo "</tr>";
                }               
            ?>
        </table>
        <a href="?c=AdminPageController&m=addNewUserForm">Add new user</a>
    </div>
</div>
