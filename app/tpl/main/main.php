<div id="app_container">
    <header id="appHeader">
        <div class="logo"><span>T-Systems</span></div>
    </header>
    
    <div id="leftContainer">                
        <div class="leftContent">  
            <div class="leftMenuItem">
                <select name="branch">
                    <option value="">Branch</option>
                </select>
            </div>
            <div class="leftMenuItem">
                <select name="department">
                    <option value="">IT</option>
                </select>
            </div>
        </div>
    </div>
    
    <div id="rightContainer">    
        <?php 
            $loginErrorMessage = $this->getLoginErrorMessage();
            if($loginErrorMessage ) echo "<p>".$loginErrorMessage."</p>";
        ?>
        <!---<a class="btn" href="<?php app\helper\GlobalHelper::base_url() ?>?c=main&m=logOut">Logout</a>-->
    </div>
</div>