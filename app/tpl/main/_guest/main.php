<div id="app_container">
    <header id="appHeader">
        <div class="logo"><span>T-Systems</span></div>
    </header>
    <div id="leftContainer">
       
        <div class="leftContent">
            <form method="post" name="loginForm">
            <input type="hidden" name="c" value="Main" />
            <input type="hidden" name="m" value="processLogin" />
            <div id="loginForm">
                <div class="leftMenuItem">
                    <label for="email">Email</label>
                    <input type="text" name="email" />
                </div>
                <div class="leftMenuItem">
                    <label for="password">Password</label>
                    <input type="passsword" name="password" />
                </div>
                <div class="leftMenuItem">
                    <input type="submit">
                </div>
            </div>
            </form>
        </div>                
        
    </div>
    <div id="rightContainer">
        <?php 
            $loginErrorMessage = $this->getLoginErrorMessage();
            if($loginErrorMessage ) echo "<p class='has-error'>".$loginErrorMessage."</p>";
        ?>
    </div>
</div>