<!DOCTYPE html>
<html>
    <head>
        <title><?php echo isset($title) ? $title : 'MVC-FrameWork'; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="public/css/style.css">
        <link rel="stylesheet" type="text/css" href="public/css/bootstrap/bootstrap.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        if (isset($cssFiles)) {
            foreach ($cssFiles AS $file) {
                echo '<link rel="stylesheet" type="text/css" href="' . $file . '">' . "\n";
            }
        }
        ?>
    </head>
    <body>
        <div id="content">
            <?php echo $pageContent; ?>
        </div>
        <script type="text/javascript" src="public/js/main.js"></script>
        <script type="text/javascript" src="public/js/jquery/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="public/js/bootstrap/bootstrap.min.js"></script>
        <?php
        if (isset($jsFiles)) {
            foreach ($jsFiles AS $file) {
                echo '<script type="text/javascript" src="' . $file . '"></script>' . "\n";
            }
        }
        if (isset($callbacks)) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . $callbacks . ');});</script>' . "\n";
        }
        ?>
    </body>
</html>
