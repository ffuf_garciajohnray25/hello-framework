<?php

namespace app\helper;

/**
 * Menu Generator
 *
 * @author John Ray Garcia <garcia.johnray25@gmail.com>
 */
class Menu {
 
    protected $menuItemsArray = array(
        '1' => array( // administrator
                'Home' => 'c=AdminPageController&m=index',        
                'User Administration' => 'c=AdminPageController&m=showUsers',
                'Events' => 'c=EventController&m=showEvents',
                'Logout' => 'c=main&m=logOut'
            ),
        '2' => array ( // content editor
                'Home' => 'c=main&m=index',
                'Events' => 'c=EventController&m=showEvents',
                'Logout' => 'c=main&m=logOut'
        ), // content editor
        '3' => array ( // default menu
                'Logout' => 'c=main&m=logOut'
            )
    );
    
    protected $menuSpecific;

    public function __construct() {
        $menuType = "3"; // default to guest
        $currentUserData = \rueckgrat\system\Sess::get("currentUserData");
        if(isset($currentUserData) && !empty($currentUserData)) {
            $menuType = $currentUserData['roleID'];
        }
        $this->menuSpecific = $this->menuItemsArray[$menuType];               
    }
    
    public function render() {
        $menuItems = $this->menuSpecific;
        $menuHtml = "<ul id='MainMenu'>";
        $i = 1;
        foreach( $menuItems as $menuLabel => $path ) {
            $indexCSSClass = '';
            if($i==1) {
                $indexCSSClass = 'indexMenu';
                $i++;
            }
            $menuHtml.= "<li><a href='?".$path."' class='$indexCSSClass'>".$menuLabel."</a></li>";
        }
        $menuHtml.="</ul>";
        return $menuHtml;
    }
}
