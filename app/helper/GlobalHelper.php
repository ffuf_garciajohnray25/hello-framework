<?php

namespace app\helper;

/**
 * Description of GlobalHelper
 *
 * @author dev
 */
class GlobalHelper {
    static function base_url() {
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        return $protocol . "://" . BASE_URL;
    }    
}
