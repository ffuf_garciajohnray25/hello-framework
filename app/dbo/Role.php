<?php

namespace app\dbo;

/**
 * Description of Role
 *
 * @author John Ray Garcia <garcia.johnray25@gmail.com>
 */
class Role extends \rueckgrat\db\Mapper {
    protected $id;
    protected $description;
    protected $name;
    
    public function __construct() {
        parent::__construct();
    }

    public function getDescription() {
        return $this->description;
    }

    public function getName() {
        return $this->name;
    }
}
