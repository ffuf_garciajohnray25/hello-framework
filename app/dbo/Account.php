<?php

namespace app\dbo;

/**
 * Description of User
 *
 * @author dev
 */
class Account extends \rueckgrat\db\Mapper {    
    
    protected $id;
    protected $forename;
    protected $lastname;
    protected $email;
    protected $gender; 
    protected $activated;
    protected $password;
    protected $salt;
    protected $role;
    protected $userAdmin;    

    public function getForename() {
        return $this->forename;
    }

    public function getLastname() {
        return $this->lastname;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getGender() {
        return $this->gender;
    }

    public function getActivated() {
        return $this->activated;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function getRole() {
        return $this->role;
    }

    public function getUserAdmin() {
        return $this->userAdmin;
    }    
}
