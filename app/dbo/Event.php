<?php

namespace app\dbo;

/**
 * Description of User
 *
 * @author dev
 */
class Event extends \rueckgrat\db\Mapper {    
    
    protected $eventEditId, $title, $motto, $host, $hostLocation, 
            $url, $subject, $keynotes, $sponsor, $feedback, $participants, 
            $startDate, $endDate, $timestamp;
    
    public function getEventEditId() {
        return $this->eventEditId;
    }

    public function setEventEditId($eventEditId) {
        $this->eventEditId = $eventEditId;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getMotto() {
        return $this->motto;
    }

    public function setMotto($motto) {
        $this->motto = $motto;
    }

    public function getHost() {
        return $this->host;
    }

    public function setHost($host) {
        $this->host = $host;
    }

    public function getHostLocation() {
        return $this->hostLocation;
    }

    public function setHostLocation($hostLocation) {
        $this->hostLocation = $hostLocation;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function getKeynotes() {
        return $this->keynotes;
    }

    public function setKeynotes($keynotes) {
        $this->keynotes = $keynotes;
    }

    public function getSponsor() {
        return $this->sponsor;
    }

    public function setSponsor($sponsor) {
        $this->sponsor = $sponsor;
    }

    public function getFeedback() {
        return $this->feedback;
    }

    public function setFeedback($feedback) {
        $this->feedback = $feedback;
    }

    public function getParticipants() {
        return $this->participants;
    }

    public function setParticipants($participants) {
        $this->participants = $participants;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    public function getTimestamp() {
        return $this->timestamp;
    }

    public function setTimestamp($timestamp) {
        $this->timestamp = $timestamp;
    }
    
}
