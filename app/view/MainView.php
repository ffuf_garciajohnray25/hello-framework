<?php

namespace app\view;

class MainView extends \rueckgrat\mvc\FastView {
    
    protected $users;
    protected $isLoggedIn;
    protected $jsFiles = array(
        'public/js/jquery/jquery-1.11.0.min.js',
        'public/js/bootstrap/bootstrap.min.js',
        'public/js/main.js'
    );
    
    public function __construct($userLoggedIn = false) {
        parent::__construct();
        
        $this->cacheDisabled = TRUE;     
        $this->isLoggedIn = $userLoggedIn;
        $this->title = "Hello Framework";
    }
    
    public function renderFrontPage($roleDataArray = NULL) {
        $guest_template = "";
        if(!$this->isLoggedIn) {
            $guest_template = "_guest/";
        }
        
        $roleDesc = !empty($roleDataArray["description"]) ? $roleDataArray["description"] : NULL;
        $this->menu = new \app\helper\Menu();                
        $this->pageContent = $this->getCompiledTpl("main/" . $guest_template ."main");
        return $this->renderMainPage();
    }
    
    public function renderAdminPage() {
        $guest_template = "";
        $this->pageContent = $this->getCompiledTpl("admin/main");
        return $this->renderMainPage();
    }


    public function viewEditForm($user) {
        $this->users = $user;
        $this->pageContent = $this->getCompiledTpl("main/editForm");
        return $this->renderMainPage();
    }
    
    protected function getLoginErrorMessage() {
        $loginErrorMessage = \rueckgrat\system\Sess::get("LoginError");
        return $loginErrorMessage;
    }
}
