<?php

namespace app\view;

class AdminPageView extends \rueckgrat\mvc\FastView {
    
    protected $accounts;
    protected $isLoggedIn;
    protected $menu;
    protected $jsFiles = array(
        'public/js/jquery/jquery-1.11.0.min.js',
        'public/js/bootstrap/bootstrap.min.js',
        'public/js/main.js'
    );
    
    public function __construct($userLoggedIn = false) {
        parent::__construct();
        
        $this->cacheDisabled = TRUE;     
        $this->isLoggedIn = $userLoggedIn;
        $this->title = "Hello Framework";
    }   
    
    public function renderAdminPage() {
        $this->pageContent = $this->getCompiledTpl("admin/home");
        $this->menu = new \app\helper\Menu();
        return $this->renderMainPage(); // The core renderMainPage fuction name seems not a good name in this case?
    }
    
    
    public function renderUserAdminPage($accounts) {
        $this->accounts = $accounts;
        $this->pageContent = $this->getCompiledTpl("admin/user_management/users");
        $this->menu = new \app\helper\Menu();
        return $this->renderMainPage();
    }

    public function renderUserForm() {
        $this->pageContent = $this->getCompiledTpl("admin/user_management/create");
        $this->menu = new \app\helper\Menu();
        return $this->renderMainPage();
    }
    
    public function viewEditForm($user) {
        $this->users = $user;
        $this->pageContent = $this->getCompiledTpl("main/editForm");
        return $this->renderMainPage();
    }  
}
