<?php

namespace app\mapper;

/**
 * Description of Account
 *
 * @author John Ray Garcia <garcia.johnray25@gmail.com>
 */
class Account extends \app\dbo\Account {
    public function __construct() {
        parent::__construct();
    }
}
