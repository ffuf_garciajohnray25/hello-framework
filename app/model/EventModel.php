<?php

namespace app\model;

/**
 * Description of Event Model
 *
 * @author johnrayg
 */
class EventModel extends \app\extend\model\AppModel {
    public function __construct() {
        parent::__construct( "event_live" );
    }      
}
