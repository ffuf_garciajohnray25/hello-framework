<?php

namespace app\model;

/**
 * Description of UserModel
 *
 * @author johnrayg
 */
class RoleModel extends \app\extend\model\AppModel {
    public function __construct() {
        parent::__construct("role");
    }    
}
