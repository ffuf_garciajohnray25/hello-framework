<?php

namespace app\model;

/**
 * Description of UserModel
 *
 * @author johnrayg
 */
class AccountModel extends \app\extend\model\AppModel {
    public function __construct() {
        parent::__construct("account");
    }
    
    public function getAllAccounts() {
        
        $res = $this->db->query("Select * from account");        
        $accounts = array();        
        while($row = $res->fetch()) {
            $account = new \app\mapper\Account();
            $account->map($row);
            $accounts[] = $account;
        }      
        
        return $accounts;
    }              
}
