<?php

namespace app\controller;

use app\extend\controller\AppController;
use rueckgrat\system\Sess;
use rueckgrat\security\Input;
use app\helper\Permission;

/**
 * @author johnrayg
 */
class Main extends AppController {
    
    protected $accountModel;
    protected $mainView;
    protected $permission;


    public function __construct() {
        parent::__construct();
        $this->accountModel = new \app\model\AccountModel();       
        $this->mainView = new \app\view\MainView();        
    }
    
    public function index() {
        //$users = $this->userModel->getAllUsers();
        
        $isLoggedIn = $this->isLoggedIn();        
        $currentUserData = $isLoggedIn ? Sess::get("currentUserData") : NULL;
       
        var_dump($_SESSION);
        
        if($isLoggedIn && $currentUserData["roleID"] == 1) { // Administrator user
           return $this->redirect( "AdminPageController", "index" );
        } else {
            return $this->mainView->renderFrontPage();
        }
    }

    public function viewUser() {
        $user = $this->userModel->getById($_GET['id']);
        return $this->mainView->viewEditForm($user);
    }    
    
    public function processLogin() {        
        $email = Input::p("email");
        $pword = Input::p("password");
        if( $userData = $this->accountModel->find( array("email" => $email, "password" => $pword) )) {
            // Initialize Session
            Sess::init();
            Sess::set("currentUserData", $userData); 
            
            // Clear prev login error if there is any
            Sess::remove("LoginError");
            $this->redirect("main", "index"); // Redirect to homepage           
        } else {
            Sess::set("LoginError", "Your username or password is incorrect, please try again.");
            $this->redirect("main", "index"); // Redirect to homepage
        }      
    }
    
    public function logOut() {        
        Sess::remove("currentUserData");
        Sess::destroy();        
        $this->redirect("main", "index"); // Redirect to homepage                
    }
    
}
