<?php

namespace app\controller;

use app\extend\controller\AppController;
use rueckgrat\system\Sess;
use rueckgrat\security\Input;
//use app\helper\Permission;

/**
 * @author John Ray Garcia <garcia.johnray25@gmail.com>
 */
class AdminPageController extends AppController {
    
    protected $accountModel;
    protected $mainView;
    protected $permission;
    protected $currentUserData;    
    
    public function __construct() {
        parent::__construct();
        var_dump($_SESSION);
        $this->accountModel = new \app\model\AccountModel();       
        $this->adminView = new \app\view\AdminPageView();
        $currentUser = Sess::get("currentUserData");
        $this->currentUserData = $currentUser;
    }
    
    public function index() {       
        return $this->adminView->renderAdminPage();        
    }
    
    public function showUsers() {
        $accounts = $this->accountModel->getAllAccounts();
        return $this->adminView->renderUserAdminPage($accounts);
    }
    
    public function addNewUserForm() {
        return $this->adminView->renderUserForm();
    }
    
    public function processAddUser() {
        //var_dump(Input::get("Forename"));
        
        
        die();
    }
}
