<?php
namespace app\validator;

use rueckgrat\security\ValidationRules;
use rueckgrat\security\ValidationRule;
/**
 * Description of AccountValidator
 *
 * @author John Ray Garcia <garcia.johnray25@gmail.com>
 */
class AccountValidator extends \rueckgrat\security\ValidatorContainer {
    public function __construct(\app\dbo\Account $account) {
        parent::__construct($account);
        
        $forename = new ValidationRule( "forename", ValidationRules::MIXED );
        $forename->setLengths(2, 50);
        $forename->setErrorMsgGlobal( "Invalid value for Forename" );        
        $this->addRule($forename);
        
        $lastname = new ValidationRule( "forename", ValidationRules::MIXED );
        $lastname->setLengths(2, 50);
        $lastname->setErrorMsgGlobal( "Invalid value for Forename" );        
        $this->addRule($forename);
        
        
    }
}
