<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" type="text/css" href="public/css/style.css" />
        <link rel="stylesheet" type="text/css" href="public/css/bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
            </head>
    <body>
        <div id="wrapper">
            <div id="app_container">
    <header id="appHeader">
        <div class="logo"><span>T-Systems</span></div>
    </header>
    
    <div id="leftContainer">                
        <div class="leftContent">  
            <div class="leftMenuItem">
                <select name="branch">
                    <option value="">Branch</option>
                </select>
            </div>
            <div class="leftMenuItem">
                <select name="department">
                    <option value="">IT</option>
                </select>
            </div>
        </div>
    </div>
    
    <div id="rightContainer">        
        <?php echo $this->menu->render() ?>
        <h3>User Management | Create User</h3>
        <form method="post">
        <input type="hidden" name="c" value="AdminPageController">
        <input type="hidden" name="m" value="processAddUser">
        <table width="800px">
            <tr>
                <td>Fore Name</td><td><input type="text" name="Forename" /></td>
            </tr>
            <tr>
                <td>Last Name</td><td><input type="text" name="Lastname" /></td>
            </tr>
            <tr>
                <td>Email</td><td><input type="text" name="Email" /></td>
            </tr>
            </tr>
                <td>Gender</td><td>Male <input type="radio" name="Gender" value="m" /> Female <input type="radio" name="Gender" value="f" /></td>                
            </tr>
            </tr>
                <td>Password</td><td><input type="password" name="password" /></td>                
            </tr>
        </table>
        <input type="submit" value="post">
        </form>
    </div>
</div>
        </div>
        <?php
        $cb = \rueckgrat\xhr\CallbackManager::getCallbacks();
        if (count($cb) > 0) {
            echo '<script type="text/javascript"">$(document).ready(function(){handleCallbacks(' . json_encode(array('callbacks' => $cb)) . ');});</script>' . "\n";
        }
        ?>
        <script type="text/javascript" src="public/js/jquery/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="public/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="public/js/main.js"></script>
    </body>
</html>